# jumbo - petstore

**requirements:**
- have Docker & Docker-Compose installed

 
**how to run**
- from the root folder run 
> docker-compose up 
- in 10 sec. readiness probe will be executed thru all layers

**how to test** 
1. GET /pet/{petId} 
- Find pet by ID
- curl -i http://localhost:8080/pet/0

2. POST /pet 
- Add a new pet to the store
- curl -H "Content-Type: application/json" -d '{"id":1,"category": {"id": 10,"name": "category_1"},"name": "testdoggie","photoUrls": ["http://www.url.com/photo1.jpeg"],"tags": [{"id": 10,"name": "tag_1"}],"status": "available"}' http://localhost:8080/pet

3. DELETE /pet/{petId}
- Deletes a pet
- curl -X DELETE http://localhost:8080/pet/0

4. PUT /pet
- Update an existing pet
- curl -X PUT -H "Content-Type: application/json" -d '{"id":1,"category": {"id": 10,"name": "category_1"},"name": "testdoggie","photoUrls": ["http://www.url.com/photo1.jpeg"],"tags": [{"id": 10,"name": "tag_1"}],"status": "taken"}' http://localhost:8080/pet

*TO BE IMPLEMENTED:*
- GET /pet/findByStatus -> Finds Pets by status
 
- POST /pet/{petId} -> Updates a pet in the store with form data

**technologies used**
- golang, grpc, docker, postgres

**architecture**
--------------------------------------------------------
- user browsers
- ...
--------------------------------------------------------
- httpgateway (shared by services)
--------------------------------------------------------
- dedicated service per domain (e.g. Pet)
- ...
--------------------------------------------------------
- dedicated DB per service (e.g. namespace pet)
- ...
--------------------------------------------------------

**what is not done**
- no unit tests

**updating proto file**
- generated proto pb files should be copied to all petproto folders of corresponding services

