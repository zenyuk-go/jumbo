module gitlab.com/zenyukgo/jumbo/petsrv

go 1.14

require (
	github.com/golang/protobuf v1.3.5
	github.com/lib/pq v1.3.0
	github.com/sirupsen/logrus v1.4.2
	google.golang.org/grpc v1.28.0
)
