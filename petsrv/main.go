package main

import (
	"database/sql"
	"net"
	"time"

	_ "github.com/lib/pq"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zenyukgo/jumbo/petsrv/petproto"
	"google.golang.org/grpc"
)

const (
	petSrvPort = ":8081"
)

type petServiceServer struct {
	petproto.UnimplementedPetServiceServer
}

var db *sql.DB

func init() {
	var err error
	time.Sleep(8 * time.Second)
	db, err = sql.Open("postgres", "postgres://postgres:pass@petdb/petstore?sslmode=disable")
	if err != nil {
		log.Error(err)
	}

	if err = db.Ping(); err != nil {
		log.Error(err)
	} else {
		db.Exec("SET search_path TO pet;")
	}
}

func main() {
	activePort, err := net.Listen("tcp", petSrvPort)
	if err != nil {
		log.Error("can not start on port ", petSrvPort)
	}

	log.Info("Pet service is serving on port ", petSrvPort)

	srv := grpc.NewServer()
	petproto.RegisterPetServiceServer(srv, &petServiceServer{})
	err = srv.Serve(activePort)
	if err != nil {
		log.Error("can not start serving Pet service")
	}

	activePort.Close()
	srv.Stop()
	db.Close()
}
